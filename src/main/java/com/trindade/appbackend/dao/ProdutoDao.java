package com.trindade.appbackend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trindade.appbackend.model.Produto;

@Repository
public interface ProdutoDao extends JpaRepository<Produto, Integer>{

}
