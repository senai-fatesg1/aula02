package com.trindade.appbackend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trindade.appbackend.dao.ProdutoDao;
import com.trindade.appbackend.model.Produto;

@RestController
@RequestMapping("/produto")
public class ProdutoRest {
	
	@Autowired
	private ProdutoDao produtoDao;
	
	@GetMapping
	public List<Produto> get() {
		return produtoDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody Produto produto) {
		produtoDao.save(produto);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		produtoDao.deleteById(id);
	}

}
